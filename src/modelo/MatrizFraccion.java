/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Admin
 */
public class MatrizFraccion implements OperacionMatriz{
    
    private Fraccion m[][];

    public MatrizFraccion() {
    }

    
    public MatrizFraccion(String fracciones_excel[][]) throws Exception
    {
    //Crear la matriz con el tamaño de las filas fracciones_excel, se deja el espacio en blanco para el espacio de las columnas:
    this.m=new Fraccion[fracciones_excel.length][];
    for(int i=0;i<this.m.length;i++)
    {
    //Para cada fila de fracciones_excel se debe obtener la cantidad de columnas para esa fila "i" y se crea las columnas:
    int cantColumnas=fracciones_excel[i].length;
    this.m[i]=new Fraccion[cantColumnas];
    /*
     Iterar para almacenar en cada columna una fracción,
    para este paso es necesario que utilice el método split de la clase String, esté método parte la cadena por un carácter
    de tal forma que:
        String algo="3/4";
        String partes[]=algo.split("/");
       El resultado sería un vector de dos posiciones:  partes={"3", "4"};
    */
    for(int j=0;j<cantColumnas;j++)
    {
        String parteFraccion[]=fracciones_excel[i][j].split("/");
        //Convertir cada parte de la cadena a su correspondiente entero (Envoltorio):
        int numerador=Integer.parseInt(parteFraccion[0]);
        int denominador=Integer.parseInt(parteFraccion[1]);
        //Crear el objeto fraccción:
        Fraccion nuevaFraccion=new Fraccion(numerador, denominador);
        //Insertar esa nueva fracción a la matriz de fracciones:
        this.m[i][j]=nuevaFraccion;
        
    }
    
    }
      
    
    }
    
    
    @Override
    public String toString() {
        String msg="";
        for(Fraccion fila[]:this.m)
        {
            for(Fraccion myF:fila)
            {
                msg+=myF.toString()+"\t ";
            }
            msg+="\n";
        }
        
       return msg; 
        
    }
    
    //Otro toString
    
    
    public String toString2() {
        String msg="";
        for(int i=0;i<this.m.length;i++)
        {
            for(int j=0;j<this.m[i].length;j++)
            {
                msg+=this.m[i][j].toString()+"\t ";
            }
            msg="\n";
        }
        
       return msg; 
        
    }
    
    /**
     * Retorna la menor fracción almacenada en la matriz
     * @return un string con la información de la fracción menor
     */
    
    public String getMenor()
    {
        return this.getMenor2().toString();
    }
    
    
    private Fraccion getMenor2()
    {
    return null;
    
    }
    /**
     * Suma dos matrices fraccionarios
     * @param dos matriz de fracción 2
     * @return una matriz con la suma resultante
     */
    private MatrizFraccion getSumar2(MatrizFraccion dos)
    {
    return null;
    }
    
    /**
     *  Suma dos matrices de fracionarios
     * @param dos matriz de fracción 2
     * @return un String que representa el toString() de la matriz de suma resultante
     */
    
    
    public String getSumar(MatrizFraccion dos)
    {
     return this.getSumar2(dos).toString();
    }
    
    /**
     *  Resta dos matrices de fracionarios
     * @param dos matriz de fracción 2
     * @return un String que representa el toString() de la matriz de resta resultante
     */
    
    public String getRestar(MatrizFraccion dos)
    {
     return this.getRestar2(dos).toString();
    }
    
    
    private MatrizFraccion getRestar2(MatrizFraccion dos)
    {
    return null;
    }
    
    
    
    /**
     * Crea una matriz de 1xM que representa la diagonal principal
     * @return un String con la matriz que almacena la diagonal principal
     */
    
    public String getDiagonalPrincipal()
    {
        
     return this.getDiagPrin().toString();
    }
    
    /**
     * Obtiene la diagonal principal
     * @return MatrizFraccion de tamaño 1xM
     */
    private MatrizFraccion getDiagPrin()
    {
    return null;
    }

    @Override
    public String getTranspuesta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getMatrizSimplificada(){
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getMultiplicación(Object matriz) {
        MatrizFraccion dos=(MatrizFraccion)matriz;
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
    
    
}
