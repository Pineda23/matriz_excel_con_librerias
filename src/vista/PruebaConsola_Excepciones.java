/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.MatrizFraccion;
import util.Matriz_Excel;

/**
 *
 * @author marco
 */
public class PruebaConsola_Excepciones {
    
    
    public static void main(String args[])
    {
    
        try {
            Matriz_Excel mm=new Matriz_Excel("data.xls");
            String matriz[][]=mm.leerExcel();
            MatrizFraccion fracciones=new MatrizFraccion(matriz);
            System.out.println(fracciones.toString());
      } catch (Exception ex) {
            System.err.println("Ha ocurrido un error:"+ex.getMessage());
        }
        
    
    }
}
